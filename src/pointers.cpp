#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a; 
  auto c = *a, d = 2 * *b;
  


  cout<<"x = "<<x<<", y = "<<y<<endl;
  cout<<"a ="<<a<<endl;
  cout<< "b ="<<b<<endl;
  cout<< "*b="<<*b<<endl;
  cout<<"d="<<d<<endl;
  cout<<"c="<<c<<endl;
  cout<< "&c="<<&c<<endl;
  cout<<"&d="<<&d<<endl;



}
